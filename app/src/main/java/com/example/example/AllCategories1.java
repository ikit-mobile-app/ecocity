package com.example.example;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.example.databinding.ActivityAllCategories1Binding;

public class AllCategories1 extends AppCompatActivity {

    ActivityAllCategories1Binding binding;
    DatabaseHelper databaseHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAllCategories1Binding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        databaseHelper = new DatabaseHelper(this);
        String login = getIntent().getStringExtra("LOGIN_USER");

        binding.imageView4All1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllCategories1.this, Menu.class);
                intent.putExtra("LOGIN_USER", login);
                startActivity(intent);
            }
        });

        binding.view24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllCategories1.this, AllCategories2.class);
                intent.putExtra("LOGIN_USER", login);
                startActivity(intent);
            }
        });

        binding.view25.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllCategories1.this, AllCategories3.class);
                intent.putExtra("LOGIN_USER", login);
                startActivity(intent);
            }
        });

        binding.view28.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllCategories1.this, MapKit.class);
                intent.putExtra("LOGIN_USER", login);
                intent.putExtra("INFO_MAP_POINT", "1");
                intent.putExtra("zoom", "17.0f");
                startActivity(intent);
            }
        });

        binding.view27.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllCategories1.this, MapKit.class);
                intent.putExtra("LOGIN_USER", login);
                intent.putExtra("INFO_MAP_POINT", "2");
                intent.putExtra("zoom", "17.0f");
                startActivity(intent);
            }
        });

        binding.view26.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllCategories1.this, MapKit.class);
                intent.putExtra("INFO_MAP_POINT", "3");
                intent.putExtra("LOGIN_USER", login);
                intent.putExtra("zoom", "17.0f");
                startActivity(intent);
            }
        });

        binding.view29.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllCategories1.this, MapKit.class);
                intent.putExtra("INFO_MAP_POINT", "4");
                intent.putExtra("LOGIN_USER", login);
                intent.putExtra("zoom", "17.0f");
                startActivity(intent);
            }
        });

        binding.view30.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllCategories1.this, MapKit.class);
                intent.putExtra("INFO_MAP_POINT", "5");
                intent.putExtra("LOGIN_USER", login);
                intent.putExtra("zoom", "17.0f");
                startActivity(intent);
            }
        });

        binding.view31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllCategories1.this, MapKit.class);
                intent.putExtra("INFO_MAP_POINT", "6");
                intent.putExtra("LOGIN_USER", login);
                intent.putExtra("zoom", "17.0f");
                startActivity(intent);
            }
        });

        binding.view32.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllCategories1.this, MapKit.class);
                intent.putExtra("INFO_MAP_POINT", "7");
                intent.putExtra("LOGIN_USER", login);
                intent.putExtra("zoom", "17.0f");
                startActivity(intent);
            }
        });

        binding.view33.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllCategories1.this, MapKit.class);
                intent.putExtra("INFO_MAP_POINT", "8");
                intent.putExtra("LOGIN_USER", login);
                intent.putExtra("zoom", "17.0f");
                startActivity(intent);
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

//    public  void goMenu(View v) {
//        Intent intent = new Intent(this, Menu.class);
//        startActivity(intent);
//    }

//    public  void goCat2(View v) {
//        Intent intent = new Intent(this, AllCategories2.class);
//        startActivity(intent);
//    }
//
//    public  void goCat3(View v) {
//        Intent intent = new Intent(this, AllCategories3.class);
//        startActivity(intent);
//    }

//    public  void goMap(View v) {
//        Intent intent = new Intent(this, MapKit.class);
//        startActivity(intent);
//    }
}