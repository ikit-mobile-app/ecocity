package com.example.example;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.example.databinding.ActivitySignUpBinding;

public class SignUp extends AppCompatActivity {

    ActivitySignUpBinding binding;
    DatabaseHelper databaseHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySignUpBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        databaseHelper = new DatabaseHelper(this);
        binding.buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String login = binding.editTextTextPersonName.getText().toString();
                String password = binding.editTextPasswordSignUp.getText().toString();
                String confirmPassword = binding.editTextPasswordSignUp2.getText().toString();
                if(login.equals("")||password.equals("")||confirmPassword.equals(""))
                    Toast.makeText(SignUp.this, "Все поля обязательны для заполнения", Toast.LENGTH_SHORT).show();
                else{
                    if(password.equals(confirmPassword)){
                        Boolean checkLogin = databaseHelper.checkLogin(login);
                        if(checkLogin == false){
                            Boolean insert = databaseHelper.insertData(login, password);
                            if(insert == true){
                                Toast.makeText(SignUp.this, "Регистрация прошла успешно!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(),SignIn.class);
                                startActivity(intent);
                            }else{
                                Toast.makeText(SignUp.this, "Регистрация не удалась!", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else{
                            Toast.makeText(SignUp.this, "Пользователь уже существует! Пожалуйста, войдите в систему", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(SignUp.this, "Неверный пароль!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        binding.textView4SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignUp.this, SignIn.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }
//    public  void goBack(View v) {
//        Intent intent = new Intent(this, SignIn.class);
//        startActivity(intent);
//    }
//    public  void signUp(View v) {
//        Intent intent = new Intent(this, SignIn.class);
//        startActivity(intent);
//    }
}