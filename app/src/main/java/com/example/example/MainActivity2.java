package com.example.example;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.example.databinding.ActivityMain2Binding;

public class MainActivity2 extends AppCompatActivity {

    ActivityMain2Binding binding;
    DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMain2Binding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        databaseHelper = new DatabaseHelper(this);
        String login = getIntent().getStringExtra("LOGIN_USER");

        binding.imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity2.this, Menu.class);
                intent.putExtra("LOGIN_USER", login);
                startActivity(intent);
            }
        });

        binding.view13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity2.this, AllCategories1.class);
                intent.putExtra("LOGIN_USER", login);
                startActivity(intent);
            }
        });

        binding.view7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity2.this, MapKit.class);
                intent.putExtra("INFO_MAP_POINT", "1");
                intent.putExtra("LOGIN_USER", login);
                intent.putExtra("zoom", "17.0f");
                startActivity(intent);
            }
        });

        binding.view19.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity2.this, MapKit.class);
                intent.putExtra("INFO_MAP_POINT", "2");
                intent.putExtra("LOGIN_USER", login);
                intent.putExtra("zoom", "17.0f");
                startActivity(intent);
            }
        });

        binding.view20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity2.this, MapKit.class);
                intent.putExtra("INFO_MAP_POINT", "3");
                intent.putExtra("LOGIN_USER", login);
                intent.putExtra("zoom", "17.0f");
                startActivity(intent);
            }
        });

        binding.view21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity2.this, MapKit.class);
                intent.putExtra("INFO_MAP_POINT", "4");
                intent.putExtra("LOGIN_USER", login);
                intent.putExtra("zoom", "17.0f");
                startActivity(intent);
            }
        });

        binding.view22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity2.this, MapKit.class);
                intent.putExtra("INFO_MAP_POINT", "9");
                intent.putExtra("LOGIN_USER", login);
                intent.putExtra("zoom", "17.0f");
                startActivity(intent);
            }
        });

        binding.view23.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity2.this, MapKit.class);
                intent.putExtra("INFO_MAP_POINT", "13");
                intent.putExtra("LOGIN_USER", login);
                intent.putExtra("zoom", "17.0f");
                startActivity(intent);
            }
        });

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

//    public  void goMenu(View v) {
//        Intent intent = new Intent(this, Menu.class);
//        startActivity(intent);
//    }

//    public  void goCategories(View v) {
//        Intent intent = new Intent(this, AllCategories1.class);
//        startActivity(intent);
//    }

//    public  void goMap(View v) {
//        Intent intent = new Intent(this, MapKit.class);
//        startActivity(intent);
//    }
}