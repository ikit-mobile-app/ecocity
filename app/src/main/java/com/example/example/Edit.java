package com.example.example;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.example.databinding.ActivityEditBinding;

public class Edit extends AppCompatActivity {

    ActivityEditBinding binding;
    DatabaseHelper databaseHelper;
    SharedPreferences mSharedPrefL;
    SharedPreferences mSharedPrefP;

    final String LAST_LOGIN = "";
    final String LAST_PASSWORD = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityEditBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        databaseHelper = new DatabaseHelper(this);
        String login = getIntent().getStringExtra("LOGIN_USER");

        mSharedPrefL = getSharedPreferences("s_login", Context.MODE_PRIVATE);
        mSharedPrefP = getSharedPreferences("s_password", Context.MODE_PRIVATE);
        String saved_login = mSharedPrefL.getString(LAST_LOGIN, "");
        String saved_pass = mSharedPrefP.getString(LAST_PASSWORD, "");

//        binding.textViewUsername.setText(login);
//        Cursor userCursor = databaseHelper.printInfoUser(login);

//        binding.editTextTextPersonName2.setText(login);

// не забывайте закрыть курсор
//        userCursor.close();

        binding.buttonEdit2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cursor cursor = databaseHelper.printInfoUser(login);
                String login = "";
                String password = "";
                String achievements = "";

                if (cursor.moveToFirst()) {
                    do {
                        login = cursor.getString(0);
                        password = cursor.getString(1);
                    }while(cursor.moveToNext());
                }
                cursor.close();

                String newLogin = binding.editTextTextPersonName2.getText().toString();
                String newPassword = binding.editTextPasswordEdit3.getText().toString();
                String newConfirmPassword = binding.editTextPasswordEdit4.getText().toString();
                if(newLogin.equals("")&&newPassword.equals("")&&newConfirmPassword.equals(""))
                    Toast.makeText(Edit.this, "Все поля остались без изменений.", Toast.LENGTH_SHORT).show();
                else{
                    if(newPassword.equals(newConfirmPassword)){
                        if (!newLogin.equals("")) {
                            login = newLogin;
                        }
                        if (!newPassword.equals("")) {
                            password = newPassword;
                        }
                        SharedPreferences.Editor mEditorL = mSharedPrefL.edit();
                        SharedPreferences.Editor mEditorP = mSharedPrefP.edit();
                        Boolean editUser = databaseHelper.editUser(login, password);
                        if(editUser == true){
                            mEditorL.putString(LAST_LOGIN, binding.editTextTextPersonName2.getText().toString());
                            mEditorL.commit();
                            mEditorP.putString(LAST_PASSWORD, binding.editTextPasswordEdit3.getText().toString());
                            mEditorP.commit();
                            Toast.makeText(Edit.this, "Обновление прошло успешно!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), Profile.class);
                            intent.putExtra("LOGIN_USER", login);
                            startActivity(intent);
                        }else{
                            Toast.makeText(Edit.this, "Обновление не удалась!", Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(Edit.this, "Неверный пароль!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        binding.textView4Edit2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Edit.this, Profile.class);
                intent.putExtra("LOGIN_USER", login);
                startActivity(intent);
            }
        });

        binding.imageView22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Edit.this, Menu.class);
                intent.putExtra("LOGIN_USER", login);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

//    public  void goMenu(View v) {
//        Intent intent = new Intent(this, Menu.class);
//        startActivity(intent);
//    }

//    public  void goBack(View v) {
//        Intent intent = new Intent(this, Profile.class);
//        startActivity(intent);
//    }
}