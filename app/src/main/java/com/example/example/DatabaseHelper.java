package com.example.example;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import androidx.annotation.Nullable;
public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String databaseName = "ecocity.db";
    private static final int DATABASE_VERSION = 1;
    public DatabaseHelper(@Nullable Context context) {
        super(context, "ecocity.db", null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase MyDatabase) {
        MyDatabase.execSQL("create Table users(id INTEGER NOT NULL UNIQUE, " +
                "login TEXT NOT NULL UNIQUE, password TEXT NOT NULL, " +
                "PRIMARY KEY(id))");

//        MyDatabase.execSQL("create Table actions(id INTEGER NOT NULL UNIQUE, " +
//                "type TEXT NOT NULL, name TEXT NOT NULL, " +
//                "icon TEXT, PRIMARY KEY(id))");
    }
    @Override
    public void onUpgrade(SQLiteDatabase MyDB, int i, int i1) {
        MyDB.execSQL("drop Table if exists users");
//        MyDB.execSQL("drop Table if exists actions");
    }
    public Boolean insertData(String login, String password){
        SQLiteDatabase MyDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("login", login);
        contentValues.put("password", password);
        long result = MyDatabase.insert("users", null, contentValues);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }
    public Boolean checkLogin(String login){
        SQLiteDatabase MyDatabase = this.getWritableDatabase();
        Cursor cursor = MyDatabase.rawQuery("Select login from users where login = ?", new String[]{login});
        if(cursor.getCount() > 0) {
            return true;
        }else {
            return false;
        }
    }
    public Boolean checkLoginPassword(String login, String password){
        SQLiteDatabase MyDatabase = this.getWritableDatabase();
        Cursor cursor = MyDatabase.rawQuery("Select login from users where login = ? and password = ?", new String[]{login, password});
        if (cursor.getCount() > 0) {
            return true;
        }else {
            return false;
        }
    }

    public Cursor printInfoUser(String login){
        SQLiteDatabase MyDatabase = this.getWritableDatabase();
        Cursor userCursor = MyDatabase.rawQuery("Select login, password from users where login = ?", new String[]{login});
        return userCursor;
    }

    public boolean deleteUser(String login) {
        SQLiteDatabase MyDatabase = this.getWritableDatabase();
        return MyDatabase.delete("users", "login = ?", new String[] {login}) > 0;
    }

    public boolean editUser(String login, String password) {
        SQLiteDatabase MyDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("login", login);
        contentValues.put("password", password);
//        contentValues.put("achievements", achievements);
        return MyDatabase.update("users", contentValues, "login=?", new String[] {login}) > 0;
    }

//    public Boolean insertData(String login, String password){
//        SQLiteDatabase MyDatabase = this.getWritableDatabase();
//        ContentValues contentValues = new ContentValues();
//        contentValues.put("login", login);
//        contentValues.put("password", password);
//        long result = MyDatabase.insert("users", null, contentValues);
//        if (result == -1) {
//            return false;
//        } else {
//            return true;
//        }
//    }

//    public Boolean insertDataActions(String login, String password){
//        SQLiteDatabase MyDatabase = this.getWritableDatabase();
//        ContentValues contentValues = new ContentValues();
//        contentValues.put("login", login);
//        contentValues.put("password", password);
//        long result = MyDatabase.insert("users", null, contentValues);
//        if (result == -1) {
//            return false;
//        } else {
//            return true;
//        }
//    }
}