package com.example.example;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;


import com.example.example.databinding.ActivitySignInBinding;

import java.io.IOException;

public class SignIn extends AppCompatActivity {

    ActivitySignInBinding binding;
    DatabaseHelper databaseHelper;
    SharedPreferences mSharedPrefL;
    SharedPreferences mSharedPrefP;

    final String LAST_LOGIN = "";
    final String LAST_PASSWORD = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySignInBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        databaseHelper = new DatabaseHelper(this);

        mSharedPrefL = getSharedPreferences("s_login", Context.MODE_PRIVATE);
        mSharedPrefP = getSharedPreferences("s_password", Context.MODE_PRIVATE);
        String saved_login = mSharedPrefL.getString(LAST_LOGIN, "");
        String saved_pass = mSharedPrefP.getString(LAST_PASSWORD, "");
        SharedPreferences sp = getSharedPreferences("my_map", Context.MODE_PRIVATE);
        sp.edit().clear().commit();

        if (!saved_login.equals("") && !saved_pass.equals("")) {
            Intent intent = new Intent(getApplicationContext(), MainActivity2.class);
            intent.putExtra("LOGIN_USER", saved_login);
            startActivity(intent);
        } else {

            binding.buttonSignIn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String login = binding.editTextPersonName.getText().toString();
                    String password = binding.editTextPassword.getText().toString();

//                String saved_login = mSharedPref.getString(LAST_LOGIN, "");
//                String saved_pass = mSharedPref.getString(LAST_PASSWORD, "");

//                if (!saved_login.equals("") && !saved_pass.equals("")) {
//                    //Присваиваем вытащенные значения полям ввода:
//                    binding.editTextPersonName.setText(saved_login);
//                    binding.editTextPassword.setText(saved_pass);
//                    Intent intent  = new Intent(getApplicationContext(), MainActivity2.class);
//                    intent.putExtra("LOGIN_USER", login);
//                    startActivity(intent);
                    if (login.equals("") || password.equals("")) {
                        Toast.makeText(SignIn.this, "Все поля обязательны для заполнения", Toast.LENGTH_SHORT).show();
                    } else {
                        SharedPreferences.Editor mEditorL = mSharedPrefL.edit();
                        SharedPreferences.Editor mEditorP = mSharedPrefP.edit();
                        Boolean checkCredentials = databaseHelper.checkLoginPassword(login, password);
                        if (checkCredentials == true) {
                            mEditorL.putString(LAST_LOGIN, binding.editTextPersonName.getText().toString());
                            mEditorL.commit();
                            mEditorP.putString(LAST_PASSWORD, binding.editTextPassword.getText().toString());
                            mEditorP.commit();
                            Toast.makeText(SignIn.this, "Вход прошел успешно!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), MainActivity2.class);
                            intent.putExtra("LOGIN_USER", login);
                            startActivity(intent);
                        } else {
                            Toast.makeText(SignIn.this, "Неверные учетные данные.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
            binding.textView4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(SignIn.this, SignUp.class);
                    startActivity(intent);
                }
            });
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }
//    public  void goSignUp(View v) {
//        Intent intent = new Intent(this, SignUp.class);
//        startActivity(intent);
//    }
//    public  void signIn(View v) {
//        Intent intent = new Intent(this, MainActivity2.class);
//        startActivity(intent);
//    }
}