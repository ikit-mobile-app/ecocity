package com.example.example;

import static android.content.ContentValues.TAG;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.FileUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.example.databinding.ActivityProfileBinding;

import java.io.File;
import java.io.IOException;

public class Profile extends AppCompatActivity {

    ActivityProfileBinding binding;
    DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityProfileBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        databaseHelper = new DatabaseHelper(this);
        String login = getIntent().getStringExtra("LOGIN_USER");

        binding.textViewUsername.setText(login);

        binding.button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                File file= new File("/data/data/com.example.example/shared_prefs/SignIn.xml");
//                file.delete();
                SharedPreferences settings1 = getSharedPreferences("s_login", Context.MODE_PRIVATE);
                settings1.edit().clear().commit();
                SharedPreferences settings2 = getSharedPreferences("s_password", Context.MODE_PRIVATE);
                settings2.edit().clear().commit();
//                SharedPreferences settings3 = getSharedPreferences("my_settings", Context.MODE_PRIVATE);
//                settings3.edit().clear().commit();
                SharedPreferences settings4 = getSharedPreferences("my_map", Context.MODE_PRIVATE);
                settings4.edit().clear().commit();
                Intent intent = new Intent(Profile.this, SignIn.class);
                startActivity(intent);
            }
        });

        binding.button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Boolean checkCredentials = databaseHelper.deleteUser(login);
                if (checkCredentials == true) {
                    SharedPreferences settings1 = getSharedPreferences("s_login", Context.MODE_PRIVATE);
                    settings1.edit().clear().commit();
                    SharedPreferences settings2 = getSharedPreferences("s_password", Context.MODE_PRIVATE);
                    settings2.edit().clear().commit();
//                    SharedPreferences settings3 = getSharedPreferences("my_settings", Context.MODE_PRIVATE);
//                    settings3.edit().clear().commit();
                    SharedPreferences settings4 = getSharedPreferences("my_map", Context.MODE_PRIVATE);
                    settings4.edit().clear().commit();
                    Toast.makeText(Profile.this, "Ваш аккаунт успешно удален.", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(Profile.this, SignIn.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(Profile.this, "Упс. Что-то пошло не так.", Toast.LENGTH_SHORT).show();
                }

            }
        });
        binding.button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Profile.this, Edit.class);
                intent.putExtra("LOGIN_USER", login);
                startActivity(intent);
            }
        });

        binding.imageView4Profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Profile.this, Menu.class);
                intent.putExtra("LOGIN_USER", login);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

//    public  void goMenu(View v) {
//        Intent intent = new Intent(this, Menu.class);
//        startActivity(intent);
//    }

//    public  void goEdit(View v) {
//        Intent intent = new Intent(this, Edit.class);
//        startActivity(intent);
//    }
}