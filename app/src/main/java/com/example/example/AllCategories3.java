package com.example.example;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.example.databinding.ActivityAllCategories3Binding;

public class AllCategories3 extends AppCompatActivity {

    ActivityAllCategories3Binding binding;
    DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAllCategories3Binding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        databaseHelper = new DatabaseHelper(this);
        String login = getIntent().getStringExtra("LOGIN_USER");

        binding.imageView4All1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllCategories3.this, Menu.class);
                intent.putExtra("LOGIN_USER", login);
                startActivity(intent);
            }
        });

        binding.view18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllCategories3.this, AllCategories1.class);
                intent.putExtra("LOGIN_USER", login);
                startActivity(intent);
            }
        });

        binding.view24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllCategories3.this, AllCategories2.class);
                intent.putExtra("LOGIN_USER", login);
                startActivity(intent);
            }
        });

        binding.view28.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllCategories3.this, MapKit.class);
                intent.putExtra("INFO_MAP_POINT", "13");
                intent.putExtra("LOGIN_USER", login);
                intent.putExtra("zoom", "17.0f");
                startActivity(intent);
            }
        });

        binding.view27.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllCategories3.this, MapKit.class);
                intent.putExtra("INFO_MAP_POINT", "14");
                intent.putExtra("LOGIN_USER", login);
                intent.putExtra("zoom", "17.0f");
                startActivity(intent);
            }
        });

        binding.view26.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllCategories3.this, MapKit.class);
                intent.putExtra("INFO_MAP_POINT", "15");
                intent.putExtra("LOGIN_USER", login);
                intent.putExtra("zoom", "17.0f");
                startActivity(intent);
            }
        });

        binding.view29.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllCategories3.this, MapKit.class);
                intent.putExtra("INFO_MAP_POINT", "16");
                intent.putExtra("LOGIN_USER", login);
                intent.putExtra("zoom", "17.0f");
                startActivity(intent);
            }
        });

        binding.view30.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllCategories3.this, MapKit.class);
                intent.putExtra("INFO_MAP_POINT", "17");
                intent.putExtra("LOGIN_USER", login);
                intent.putExtra("zoom", "17.0f");
                startActivity(intent);
            }
        });

        binding.view31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllCategories3.this, MapKit.class);
                intent.putExtra("INFO_MAP_POINT", "18");
                intent.putExtra("LOGIN_USER", login);
                intent.putExtra("zoom", "17.0f");
                startActivity(intent);
            }
        });

        binding.view32.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllCategories3.this, MapKit.class);
                intent.putExtra("INFO_MAP_POINT", "19");
                intent.putExtra("LOGIN_USER", login);
                intent.putExtra("zoom", "17.0f");
                startActivity(intent);
            }
        });

        binding.view33.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllCategories3.this, MapKit.class);
                intent.putExtra("INFO_MAP_POINT", "20");
                intent.putExtra("LOGIN_USER", login);
                intent.putExtra("zoom", "17.0f");
                startActivity(intent);
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

//    public  void goMenu(View v) {
//        Intent intent = new Intent(this, Menu.class);
//        startActivity(intent);
//    }
//
//    public  void goCat2(View v) {
//        Intent intent = new Intent(this, AllCategories2.class);
//        startActivity(intent);
//    }
//
//    public  void goCat1(View v) {
//        Intent intent = new Intent(this, AllCategories1.class);
//        startActivity(intent);
//    }

//    public  void goMap(View v) {
//        Intent intent = new Intent(this, MapKit.class);
//        startActivity(intent);
//    }
}