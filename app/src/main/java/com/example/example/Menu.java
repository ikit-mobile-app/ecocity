package com.example.example;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.example.databinding.ActivityMenuBinding;

public class Menu extends AppCompatActivity {

    ActivityMenuBinding binding;
    DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMenuBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        databaseHelper = new DatabaseHelper(this);
        String login = getIntent().getStringExtra("LOGIN_USER");

        binding.view8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Menu.this, Profile.class);
                intent.putExtra("LOGIN_USER", login);
                startActivity(intent);
            }
        });

        binding.view16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Menu.this, MainActivity2.class);
                intent.putExtra("LOGIN_USER", login);
                startActivity(intent);
            }
        });

        binding.view12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Menu.this, AllCategories1.class);
                intent.putExtra("LOGIN_USER", login);
                startActivity(intent);
            }
        });

        binding.view14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Menu.this, News.class);
                intent.putExtra("LOGIN_USER", login);
                startActivity(intent);
            }
        });

        binding.view15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Menu.this, MapKit.class);
                intent.putExtra("INFO_MAP_POINT", "21");
                intent.putExtra("LOGIN_USER", login);
                intent.putExtra("zoom", "13.0f");
                startActivity(intent);
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

//    public  void goNews(View v) {
//        Intent intent = new Intent(this, News.class);
//        startActivity(intent);
//    }

//    public  void goProfile(View v) {
//        Intent intent = new Intent(this, Profile.class);
//        startActivity(intent);
//    }
//    public  void goHome(View v) {
//        Intent intent = new Intent(this, MainActivity2.class);
//        startActivity(intent);
//    }

//    public  void goCategories(View v) {
//        Intent intent = new Intent(this, AllCategories1.class);
//        startActivity(intent);
//    }

//    public  void goMap(View v) {
//        Intent intent = new Intent(this, MapKit.class);
//        startActivity(intent);
//    }
}