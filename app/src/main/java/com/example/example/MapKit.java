package com.example.example;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

//import com.example.example.databinding.ActivityMapKitBinding;
import com.yandex.mapkit.Animation;
import com.yandex.mapkit.MapKitFactory;
import com.yandex.mapkit.geometry.Point;
import com.yandex.mapkit.map.CameraPosition;
import com.yandex.mapkit.map.MapObject;
import com.yandex.mapkit.map.MapObjectCollection;
import com.yandex.mapkit.map.MapObjectTapListener;
import com.yandex.mapkit.mapview.MapView;
import com.yandex.runtime.image.ImageProvider;

public class MapKit extends AppCompatActivity implements MapObjectTapListener {
    private MapView mapView;
    private final String MAPKIT_API_KEY = "2e5bef5b-d686-4c54-b3c9-824090351c61";
    private static final String MY_MAP = "my_map";
    private boolean hasVisited;
//    private float zoom = 17.0f;
    private float zoom;
    private String infoMapPoint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        SharedPreferences sp = getSharedPreferences(MY_MAP, Context.MODE_PRIVATE);
        // проверяем, первый ли раз открывается программа
        hasVisited = sp.getBoolean("hasVisited", false);

        if (!hasVisited) {
            MapKitFactory.setApiKey(MAPKIT_API_KEY);
            SharedPreferences.Editor e = sp.edit();
            e.putBoolean("hasVisited", true);
            e.commit();
        }
        MapKitFactory.initialize(this);

//        Button button1 = (Button)findViewById(R.id.plus);
//        Button button2 = (Button) findViewById(R.id.minus);

//        zoom = 17.0f;
        zoom = Float.parseFloat(getIntent().getStringExtra("zoom"));

        setContentView(R.layout.activity_map_kit);
        mapView = (MapView)findViewById(R.id.mapview);
//        mapView.getMap().move(
//                new CameraPosition(new Point(56.0184, 92.8672), zoom, 0.0f, 0.0f),
//                new Animation(Animation.Type.SMOOTH, 0),
//                null);

        // Маркеры
        Bitmap bitmap1 = this.getBitmapFromVectorDrawable(this, R.drawable.map_icon1);
        Bitmap bitmap2 = this.getBitmapFromVectorDrawable(this, R.drawable.map_icon2);
        Bitmap bitmap3 = this.getBitmapFromVectorDrawable(this, R.drawable.map_icon3);
//        userLocationView.pin.setIcon(ImageProvider.fromBitmap(bitmap))
        MapObjectCollection pointCollection = mapView.getMap().getMapObjects().addCollection();
        pointCollection.addTapListener(this);
        infoMapPoint = getIntent().getStringExtra("INFO_MAP_POINT");
        // В дальнейшем оптимизировать код для создания меток!
        if (infoMapPoint.equals("1")) {
            // Сдать батарейки
            //Копылова, 66
            Point mappoint1= new Point(56.012803, 92.815193);
            mapView.getMap().move(
                    new CameraPosition(mappoint1, zoom, 0.0f, 0.0f),
                    new Animation(Animation.Type.SMOOTH, 0),
                    null);
            pointCollection.addPlacemark(mappoint1, ImageProvider.fromBitmap(bitmap1)).setUserData("Копылова, 66");
            // Свободный проспект, 42
            Point mappoint2= new Point(56.023611, 92.807009);
            pointCollection.addPlacemark(mappoint2, ImageProvider.fromBitmap(bitmap1)).setUserData("Свободный проспект, 42");
            // Тотмина, 6
            Point mappoint3= new Point(56.031553, 92.775766);
            pointCollection.addPlacemark(mappoint3, ImageProvider.fromBitmap(bitmap1)).setUserData("Тотмина, 6");
            // Калинина, 82
            Point mappoint4= new Point(56.042296, 92.783743);
            pointCollection.addPlacemark(mappoint4, ImageProvider.fromBitmap(bitmap1)).setUserData("Калинина, 82");
            // Свердловская 293
            Point mappoint5= new Point(55.964438, 92.729161);
            pointCollection.addPlacemark(mappoint5, ImageProvider.fromBitmap(bitmap1)).setUserData("Свердловская 293");
            // Деповская улица, 37А
            Point mappoint6= new Point(56.008727, 92.832476);
            pointCollection.addPlacemark(mappoint6, ImageProvider.fromBitmap(bitmap1)).setUserData("Деповская улица, 37А");
            // Бограда, 144А
            Point mappoint7= new Point(56.006653, 92.832773);
            pointCollection.addPlacemark(mappoint7, ImageProvider.fromBitmap(bitmap1)).setUserData("Бограда, 144А");
            //Бограда, 134
            Point mappoint8= new Point(56.006628, 92.836159);
            pointCollection.addPlacemark(mappoint8, ImageProvider.fromBitmap(bitmap1)).setUserData("Бограда, 134");
            // Красной Армии, 10
            Point mappoint9= new Point(56.014131, 92.855949);
            pointCollection.addPlacemark(mappoint9, ImageProvider.fromBitmap(bitmap1)).setUserData("Красной Армии, 10");
            // Мира, 96
            Point mappoint10= new Point(56.011761, 92.861860);
            pointCollection.addPlacemark(mappoint10, ImageProvider.fromBitmap(bitmap1)).setUserData("Мира, 96");
            // Ленина, 88
            Point mappoint11= new Point(56.013432, 92.866873);
            pointCollection.addPlacemark(mappoint11, ImageProvider.fromBitmap(bitmap1)).setUserData("Ленина, 88");
            // Карла Маркса, 95, к.1
            Point mappoint12= new Point(56.009980, 92.870547);
            pointCollection.addPlacemark(mappoint12, ImageProvider.fromBitmap(bitmap1)).setUserData("Карла Маркса, 95, к.1");
            // Кирова, 19
            Point mappoint13= new Point(56.010946, 92.863378);
            pointCollection.addPlacemark(mappoint13, ImageProvider.fromBitmap(bitmap1)).setUserData("Кирова, 19");
            // Карла Маркса, 49
            Point mappoint14= new Point(56.010604, 92.880069);
            pointCollection.addPlacemark(mappoint14, ImageProvider.fromBitmap(bitmap1)).setUserData("Карла Маркса, 49");
            // Щорса, 43
            Point mappoint15= new Point(55.990796, 92.953821);
            pointCollection.addPlacemark(mappoint15, ImageProvider.fromBitmap(bitmap1)).setUserData("Щорса, 43");
            // 9 мая, 77
            Point mappoint16= new Point(56.050918, 92.904378);
            pointCollection.addPlacemark(mappoint16, ImageProvider.fromBitmap(bitmap1)).setUserData("9 мая, 77");
        } else if (infoMapPoint.equals("2")) {
            // Сдать макулатуру
            // Свободный,80
            Point mappoint1 = new Point(56.005349, 92.768939);
            mapView.getMap().move(
                    new CameraPosition(mappoint1, zoom, 0.0f, 0.0f),
                    new Animation(Animation.Type.SMOOTH, 0),
                    null);
            pointCollection.addPlacemark(mappoint1, ImageProvider.fromBitmap(bitmap1)).setUserData("Свободный,80");
            // 60 лет Октября, 2
            Point mappoint2 = new Point(55.975783, 92.835908);
            pointCollection.addPlacemark(mappoint2, ImageProvider.fromBitmap(bitmap1)).setUserData("60 лет Октября, 2");
            // Ленина, 88
            Point mappoint3 = new Point(56.013432, 92.866873);
            pointCollection.addPlacemark(mappoint3, ImageProvider.fromBitmap(bitmap1)).setUserData("Ленина, 88");
            // Линейная, 114
            Point mappoint4 = new Point(56.042557, 92.879827);
            pointCollection.addPlacemark(mappoint4, ImageProvider.fromBitmap(bitmap1)).setUserData("Линейная, 114");
            // Светлогорская, 35
            Point mappoint5 = new Point(56.063801, 92.909633);
            pointCollection.addPlacemark(mappoint5, ImageProvider.fromBitmap(bitmap1)).setUserData("Светлогорская, 35");
            // Богдана Хмельницкого, 4с7
            Point mappoint6 = new Point(56.006059, 92.998997);
            pointCollection.addPlacemark(mappoint6, ImageProvider.fromBitmap(bitmap1)).setUserData("Богдана Хмельницкого, 4с7");
        } else if (infoMapPoint.equals("3")) {
            // Сдать пластик
            // Академгородок, 66
            Point mappoint1 = new Point(55.987382, 92.774715);
            mapView.getMap().move(
                    new CameraPosition(mappoint1, zoom, 0.0f, 0.0f),
                    new Animation(Animation.Type.SMOOTH, 0),
                    null);
            pointCollection.addPlacemark(mappoint1, ImageProvider.fromBitmap(bitmap1)).setUserData("Академгородок, 66");
            // Борисова, 22
            Point mappoint2 = new Point(55.994280, 92.795026);
            pointCollection.addPlacemark(mappoint2, ImageProvider.fromBitmap(bitmap1)).setUserData("Борисова, 22");
            // Пушкина,32
            Point mappoint3 = new Point(56.011696, 92.819065);
            pointCollection.addPlacemark(mappoint3, ImageProvider.fromBitmap(bitmap1)).setUserData("Пушкина,32");
            // Мира, 29
            Point mappoint4 = new Point(56.012425, 92.881947);
            pointCollection.addPlacemark(mappoint4, ImageProvider.fromBitmap(bitmap1)).setUserData("Мира, 29");
            // Парижской Коммуны, 9
            Point mappoint5 = new Point(56.010146, 92.880967);
            pointCollection.addPlacemark(mappoint5, ImageProvider.fromBitmap(bitmap1)).setUserData("Парижской Коммуны, 9");
            // Белинского, 1
            Point mappoint6 = new Point(56.018766, 92.896383);
            pointCollection.addPlacemark(mappoint6, ImageProvider.fromBitmap(bitmap1)).setUserData("Белинского, 1");
        } else if (infoMapPoint.equals("4")) {
            // Сдать стекло
            // Урицкого, 41
            Point mappoint1 = new Point(56.009114, 92.880330);
            mapView.getMap().move(
                    new CameraPosition(mappoint1, zoom, 0.0f, 0.0f),
                    new Animation(Animation.Type.SMOOTH, 0),
                    null);
            pointCollection.addPlacemark(mappoint1, ImageProvider.fromBitmap(bitmap1)).setUserData("Урицкого, 41");
            // Борисова, 22
            Point mappoint2 = new Point(55.994280, 92.795026);
            pointCollection.addPlacemark(mappoint2, ImageProvider.fromBitmap(bitmap1)).setUserData("Борисова, 22");
            // Ленина, 43
            Point mappoint3 = new Point(56.013714, 92.877230);
            pointCollection.addPlacemark(mappoint3, ImageProvider.fromBitmap(bitmap1)).setUserData("Ленина, 43");
            // Карла Маркса, 102
            Point mappoint4 = new Point(56.010342, 92.860432);
            pointCollection.addPlacemark(mappoint4, ImageProvider.fromBitmap(bitmap1)).setUserData("Карла Маркса, 102");
            // Республики, 33А
            Point mappoint5 = new Point(56.015877, 92.852841);
            pointCollection.addPlacemark(mappoint5, ImageProvider.fromBitmap(bitmap1)).setUserData("Республики, 33А");
            // Белинского, 1
            Point mappoint6 = new Point(56.018766, 92.896383);
            pointCollection.addPlacemark(mappoint6, ImageProvider.fromBitmap(bitmap1)).setUserData("Белинского, 1");
        } else if (infoMapPoint.equals("5")) {
            // Сдать другие виды отходов
            // Урицкого, 41
            Point mappoint1 = new Point(56.009114, 92.880330);
            mapView.getMap().move(
                    new CameraPosition(mappoint1, zoom, 0.0f, 0.0f),
                    new Animation(Animation.Type.SMOOTH, 0),
                    null);
            pointCollection.addPlacemark(mappoint1, ImageProvider.fromBitmap(bitmap1)).setUserData("Урицкого, 41");
            // Борисова, 22
            Point mappoint2 = new Point(55.994280, 92.795026);
            pointCollection.addPlacemark(mappoint2, ImageProvider.fromBitmap(bitmap1)).setUserData("Борисова, 22");
            // Ленина, 43
            Point mappoint3 = new Point(56.013714, 92.877230);
            pointCollection.addPlacemark(mappoint3, ImageProvider.fromBitmap(bitmap1)).setUserData("Ленина, 43");
            // Карла Маркса, 102
            Point mappoint4 = new Point(56.010342, 92.860432);
            pointCollection.addPlacemark(mappoint4, ImageProvider.fromBitmap(bitmap1)).setUserData("Карла Маркса, 102");
            // Республики, 33А
            Point mappoint5 = new Point(56.015877, 92.852841);
            pointCollection.addPlacemark(mappoint5, ImageProvider.fromBitmap(bitmap1)).setUserData("Республики, 33А");
            // Белинского, 1
            Point mappoint6 = new Point(56.018766, 92.896383);
            pointCollection.addPlacemark(mappoint6, ImageProvider.fromBitmap(bitmap1)).setUserData("Белинского, 1");
        } else if (infoMapPoint.equals("6")) {
            // Сдать мелочь
            // Академгородок, 66
            Point mappoint1 = new Point(55.987382, 92.774715);
            mapView.getMap().move(
                    new CameraPosition(mappoint1, zoom, 0.0f, 0.0f),
                    new Animation(Animation.Type.SMOOTH, 0),
                    null);
            pointCollection.addPlacemark(mappoint1, ImageProvider.fromBitmap(bitmap1)).setUserData("Академгородок, 66");
            // Борисова, 22
            Point mappoint2 = new Point(55.994280, 92.795026);
            pointCollection.addPlacemark(mappoint2, ImageProvider.fromBitmap(bitmap1)).setUserData("Борисова, 22");
            // Пушкина,32
            Point mappoint3 = new Point(56.011696, 92.819065);
            pointCollection.addPlacemark(mappoint3, ImageProvider.fromBitmap(bitmap1)).setUserData("Пушкина,32");
            // Мира, 29
            Point mappoint4 = new Point(56.012425, 92.881947);
            pointCollection.addPlacemark(mappoint4, ImageProvider.fromBitmap(bitmap1)).setUserData("Мира, 29");
            // Парижской Коммуны, 9
            Point mappoint5 = new Point(56.010146, 92.880967);
            pointCollection.addPlacemark(mappoint5, ImageProvider.fromBitmap(bitmap1)).setUserData("Парижской Коммуны, 9");
            // Белинского, 1
            Point mappoint6 = new Point(56.018766, 92.896383);
            pointCollection.addPlacemark(mappoint6, ImageProvider.fromBitmap(bitmap1)).setUserData("Белинского, 1");
        } else if (infoMapPoint.equals("7")) {
            // Сдать крышки
            // Академгородок, 66
            Point mappoint1 = new Point(55.987382, 92.774715);
            mapView.getMap().move(
                    new CameraPosition(mappoint1, zoom, 0.0f, 0.0f),
                    new Animation(Animation.Type.SMOOTH, 0),
                    null);
            pointCollection.addPlacemark(mappoint1, ImageProvider.fromBitmap(bitmap1)).setUserData("Академгородок, 66");
            // Борисова, 22
            Point mappoint2 = new Point(55.994280, 92.795026);
            pointCollection.addPlacemark(mappoint2, ImageProvider.fromBitmap(bitmap1)).setUserData("Борисова, 22");
            // Пушкина,32
            Point mappoint3 = new Point(56.011696, 92.819065);
            pointCollection.addPlacemark(mappoint3, ImageProvider.fromBitmap(bitmap1)).setUserData("Пушкина,32");
            // Мира, 29
            Point mappoint4 = new Point(56.012425, 92.881947);
            pointCollection.addPlacemark(mappoint4, ImageProvider.fromBitmap(bitmap1)).setUserData("Мира, 29");
            // Парижской Коммуны, 9
            Point mappoint5 = new Point(56.010146, 92.880967);
            pointCollection.addPlacemark(mappoint5, ImageProvider.fromBitmap(bitmap1)).setUserData("Парижской Коммуны, 9");
            // Белинского, 1
            Point mappoint6 = new Point(56.018766, 92.896383);
            pointCollection.addPlacemark(mappoint6, ImageProvider.fromBitmap(bitmap1)).setUserData("Белинского, 1");
        } else if (infoMapPoint.equals("8")) {
            // Раздельно сдать вторсырье
            // Урицкого, 41
            Point mappoint1 = new Point(56.009114, 92.880330);
            mapView.getMap().move(
                    new CameraPosition(mappoint1, zoom, 0.0f, 0.0f),
                    new Animation(Animation.Type.SMOOTH, 0),
                    null);
            pointCollection.addPlacemark(mappoint1, ImageProvider.fromBitmap(bitmap1)).setUserData("Урицкого, 41");
            // Борисова, 22
            Point mappoint2 = new Point(55.994280, 92.795026);
            pointCollection.addPlacemark(mappoint2, ImageProvider.fromBitmap(bitmap1)).setUserData("Борисова, 22");
            // Ленина, 43
            Point mappoint3 = new Point(56.013714, 92.877230);
            pointCollection.addPlacemark(mappoint3, ImageProvider.fromBitmap(bitmap1)).setUserData("Ленина, 43");
            // Карла Маркса, 102
            Point mappoint4 = new Point(56.010342, 92.860432);
            pointCollection.addPlacemark(mappoint4, ImageProvider.fromBitmap(bitmap1)).setUserData("Карла Маркса, 102");
            // Республики, 33А
            Point mappoint5 = new Point(56.015877, 92.852841);
            pointCollection.addPlacemark(mappoint5, ImageProvider.fromBitmap(bitmap1)).setUserData("Республики, 33А");
            // Белинского, 1
            Point mappoint6 = new Point(56.018766, 92.896383);
            pointCollection.addPlacemark(mappoint6, ImageProvider.fromBitmap(bitmap1)).setUserData("Белинского, 1");
        } else if (infoMapPoint.equals("9")) {
            // Помощь животным
            // Айвазовского, 37
            Point mappoint1 = new Point(56.023374, 93.042646);
            mapView.getMap().move(
                    new CameraPosition(mappoint1, zoom, 0.0f, 0.0f),
                    new Animation(Animation.Type.SMOOTH, 0),
                    null);
            pointCollection.addPlacemark(mappoint1, ImageProvider.fromBitmap(bitmap2)).setUserData("Айвазовского, 37");
            // Свободный, 29
            Point mappoint2 = new Point(56.023459, 92.822281);
            pointCollection.addPlacemark(mappoint2, ImageProvider.fromBitmap(bitmap2)).setUserData("Свободный, 29");
        } else if (infoMapPoint.equals("10")) {
            // Стать волонтером(животные)
            // Айвазовского, 37
            Point mappoint1 = new Point(56.023374, 93.042646);
            mapView.getMap().move(
                    new CameraPosition(mappoint1, zoom, 0.0f, 0.0f),
                    new Animation(Animation.Type.SMOOTH, 0),
                    null);
            pointCollection.addPlacemark(mappoint1, ImageProvider.fromBitmap(bitmap2)).setUserData("Айвазовского, 37");
            // Свободный, 29
            Point mappoint2 = new Point(56.023459, 92.822281);
            pointCollection.addPlacemark(mappoint2, ImageProvider.fromBitmap(bitmap2)).setUserData("Свободный, 29");
        } else if (infoMapPoint.equals("11")) {
            // Забрать животное домой
            // Айвазовского, 37
            Point mappoint1 = new Point(56.023374, 93.042646);
            mapView.getMap().move(
                    new CameraPosition(mappoint1, zoom, 0.0f, 0.0f),
                    new Animation(Animation.Type.SMOOTH, 0),
                    null);
            pointCollection.addPlacemark(mappoint1, ImageProvider.fromBitmap(bitmap2)).setUserData("Айвазовского, 37");
            // Свободный, 29
            Point mappoint2 = new Point(56.023459, 92.822281);
            pointCollection.addPlacemark(mappoint2, ImageProvider.fromBitmap(bitmap2)).setUserData("Свободный, 29");
        } else if (infoMapPoint.equals("12")) {
            // Места для выгула собак
            // Татышев
            Point mappoint1 = new Point(56.033744, 92.942090);
            mapView.getMap().move(
                    new CameraPosition(mappoint1, zoom, 0.0f, 0.0f),
                    new Animation(Animation.Type.SMOOTH, 0),
                    null);
            pointCollection.addPlacemark(mappoint1, ImageProvider.fromBitmap(bitmap2)).setUserData("Татышев");
            // Покровский парк
            Point mappoint2 = new Point(56.024426, 92.863553);
            pointCollection.addPlacemark(mappoint2, ImageProvider.fromBitmap(bitmap2)).setUserData("Покровский парк");
            // 2-й микрорайон
            Point mappoint3 = new Point(56.046360, 92.903389);
            pointCollection.addPlacemark(mappoint3, ImageProvider.fromBitmap(bitmap2)).setUserData("2-й микрорайон");
            // Академика Вавилова, 39
            Point mappoint4 = new Point(55.999646, 92.939367);
            pointCollection.addPlacemark(mappoint4, ImageProvider.fromBitmap(bitmap2)).setUserData("Академика Вавилова, 39");
            // Кольцевая улица
            Point mappoint5 = new Point(55.994028, 92.903190);
            pointCollection.addPlacemark(mappoint5, ImageProvider.fromBitmap(bitmap2)).setUserData("Кольцевая улица");
        } else if (infoMapPoint.equals("13")) {
            // Стать волонтером(люди)
            // Красномосковская улица, 42
            Point mappoint1 = new Point(56.023089, 92.820814);
            mapView.getMap().move(
                    new CameraPosition(mappoint1, zoom, 0.0f, 0.0f),
                    new Animation(Animation.Type.SMOOTH, 0),
                    null);
            pointCollection.addPlacemark(mappoint1, ImageProvider.fromBitmap(bitmap3)).setUserData("Красномосковская улица, 42");
            // Ады Лебедевой, 149
            Point mappoint2 = new Point(56.014575, 92.842211);
            pointCollection.addPlacemark(mappoint2, ImageProvider.fromBitmap(bitmap3)).setUserData("Ады Лебедевой, 149");
        } else if (infoMapPoint.equals("14")) {
            // Помощь городу
            // Красномосковская улица, 42
            Point mappoint1 = new Point(56.023089, 92.820814);
            mapView.getMap().move(
                    new CameraPosition(mappoint1, zoom, 0.0f, 0.0f),
                    new Animation(Animation.Type.SMOOTH, 0),
                    null);
            pointCollection.addPlacemark(mappoint1, ImageProvider.fromBitmap(bitmap3)).setUserData("Красномосковская улица, 42");
            // Ады Лебедевой, 149
            Point mappoint2 = new Point(56.014575, 92.842211);
            pointCollection.addPlacemark(mappoint2, ImageProvider.fromBitmap(bitmap3)).setUserData("Ады Лебедевой, 149");
            // Карла Маркса, 93
            Point mappoint3 = new Point(56.009157, 92.873260);
            pointCollection.addPlacemark(mappoint3, ImageProvider.fromBitmap(bitmap3)).setUserData("Карла Маркса, 93");
            // Карла Маркса, 49
            Point mappoint4 = new Point(56.010658, 92.880486);
            pointCollection.addPlacemark(mappoint4, ImageProvider.fromBitmap(bitmap3)).setUserData("Карла Маркса, 49");
        } else if (infoMapPoint.equals("15")) {
            // Стать донором
            // Партизана Железняка, 3З
            Point mappoint1 = new Point(56.027760, 92.913468);
            mapView.getMap().move(
                    new CameraPosition(mappoint1, zoom, 0.0f, 0.0f),
                    new Animation(Animation.Type.SMOOTH, 0),
                    null);
            pointCollection.addPlacemark(mappoint1, ImageProvider.fromBitmap(bitmap3)).setUserData("Партизана Железняка, 3З");
        } else if (infoMapPoint.equals("16")) {
            // Помощь пропавшим людям
            // Красномосковская улица, 42
            Point mappoint1 = new Point(56.023089, 92.820814);
            mapView.getMap().move(
                    new CameraPosition(mappoint1, zoom, 0.0f, 0.0f),
                    new Animation(Animation.Type.SMOOTH, 0),
                    null);
            pointCollection.addPlacemark(mappoint1, ImageProvider.fromBitmap(bitmap3)).setUserData("Красномосковская улица, 42");
            // Ады Лебедевой, 149
            Point mappoint2 = new Point(56.014575, 92.842211);
            pointCollection.addPlacemark(mappoint2, ImageProvider.fromBitmap(bitmap3)).setUserData("Ады Лебедевой, 149");
            // Карла Маркса, 93
            Point mappoint3 = new Point(56.009157, 92.873260);
            pointCollection.addPlacemark(mappoint3, ImageProvider.fromBitmap(bitmap3)).setUserData("Карла Маркса, 93");
            // Карла Маркса, 49
            Point mappoint4 = new Point(56.010658, 92.880486);
            pointCollection.addPlacemark(mappoint4, ImageProvider.fromBitmap(bitmap3)).setUserData("Карла Маркса, 49");
        } else if (infoMapPoint.equals("17")) {
            // Помощь пожилым людям
            // Красномосковская улица, 42
            Point mappoint1 = new Point(56.023089, 92.820814);
            mapView.getMap().move(
                    new CameraPosition(mappoint1, zoom, 0.0f, 0.0f),
                    new Animation(Animation.Type.SMOOTH, 0),
                    null);
            pointCollection.addPlacemark(mappoint1, ImageProvider.fromBitmap(bitmap3)).setUserData("Красномосковская улица, 42");
            // Ады Лебедевой, 149
            Point mappoint2 = new Point(56.014575, 92.842211);
            pointCollection.addPlacemark(mappoint2, ImageProvider.fromBitmap(bitmap3)).setUserData("Ады Лебедевой, 149");
            // Карла Маркса, 93
            Point mappoint3 = new Point(56.009157, 92.873260);
            pointCollection.addPlacemark(mappoint3, ImageProvider.fromBitmap(bitmap3)).setUserData("Карла Маркса, 93");
            // Карла Маркса, 49
            Point mappoint4 = new Point(56.010658, 92.880486);
            pointCollection.addPlacemark(mappoint4, ImageProvider.fromBitmap(bitmap3)).setUserData("Карла Маркса, 49");
        } else if (infoMapPoint.equals("18")) {
            // Помощь детям
            // Красномосковская улица, 42
            Point mappoint1 = new Point(56.023089, 92.820814);
            mapView.getMap().move(
                    new CameraPosition(mappoint1, zoom, 0.0f, 0.0f),
                    new Animation(Animation.Type.SMOOTH, 0),
                    null);
            pointCollection.addPlacemark(mappoint1, ImageProvider.fromBitmap(bitmap3)).setUserData("Красномосковская улица, 42");
            // Ады Лебедевой, 149
            Point mappoint2 = new Point(56.014575, 92.842211);
            pointCollection.addPlacemark(mappoint2, ImageProvider.fromBitmap(bitmap3)).setUserData("Ады Лебедевой, 149");
            // Карла Маркса, 93
            Point mappoint3 = new Point(56.009157, 92.873260);
            pointCollection.addPlacemark(mappoint3, ImageProvider.fromBitmap(bitmap3)).setUserData("Карла Маркса, 93");
            // Карла Маркса, 49
            Point mappoint4 = new Point(56.010658, 92.880486);
            pointCollection.addPlacemark(mappoint4, ImageProvider.fromBitmap(bitmap3)).setUserData("Карла Маркса, 49");
        } else if (infoMapPoint.equals("19")) {
            // Отдать ненужную одежду
            // Красномосковская улица, 42
            Point mappoint1 = new Point(56.023089, 92.820814);
            mapView.getMap().move(
                    new CameraPosition(mappoint1, zoom, 0.0f, 0.0f),
                    new Animation(Animation.Type.SMOOTH, 0),
                    null);
            pointCollection.addPlacemark(mappoint1, ImageProvider.fromBitmap(bitmap3)).setUserData("Красномосковская улица, 42");
            // Ады Лебедевой, 149
            Point mappoint2 = new Point(56.014575, 92.842211);
            pointCollection.addPlacemark(mappoint2, ImageProvider.fromBitmap(bitmap3)).setUserData("Ады Лебедевой, 149");
            // Карла Маркса, 93
            Point mappoint3 = new Point(56.009157, 92.873260);
            pointCollection.addPlacemark(mappoint3, ImageProvider.fromBitmap(bitmap3)).setUserData("Карла Маркса, 93");
            // Карла Маркса, 49
            Point mappoint4 = new Point(56.010658, 92.880486);
            pointCollection.addPlacemark(mappoint4, ImageProvider.fromBitmap(bitmap3)).setUserData("Карла Маркса, 49");
        } else if (infoMapPoint.equals("20")) {
            // Пожертвовать деньги в фонд
            // Красномосковская улица, 42
            Point mappoint1 = new Point(56.023089, 92.820814);
            mapView.getMap().move(
                    new CameraPosition(mappoint1, zoom, 0.0f, 0.0f),
                    new Animation(Animation.Type.SMOOTH, 0),
                    null);
            pointCollection.addPlacemark(mappoint1, ImageProvider.fromBitmap(bitmap3)).setUserData("Красномосковская улица, 42");
            // Ады Лебедевой, 149
            Point mappoint2 = new Point(56.014575, 92.842211);
            pointCollection.addPlacemark(mappoint2, ImageProvider.fromBitmap(bitmap3)).setUserData("Ады Лебедевой, 149");
            // Карла Маркса, 93
            Point mappoint3 = new Point(56.009157, 92.873260);
            pointCollection.addPlacemark(mappoint3, ImageProvider.fromBitmap(bitmap3)).setUserData("Карла Маркса, 93");
            // Карла Маркса, 49
            Point mappoint4 = new Point(56.010658, 92.880486);
            pointCollection.addPlacemark(mappoint4, ImageProvider.fromBitmap(bitmap3)).setUserData("Карла Маркса, 49");
        } else {
            // Все вместе
//            Point mappoint2 = new Point(56.005551, 92.849005);
//            mapView.getMap().getMapObjects().addPlacemark(mappoint2);
            mapView.getMap().move(
                    new CameraPosition(new Point(56.0184, 92.8672), zoom, 0.0f, 0.0f),
                    new Animation(Animation.Type.SMOOTH, 0),
                    null);
            //Копылова, 66
            Point mappoint1= new Point(56.012803, 92.815193);
            pointCollection.addPlacemark(mappoint1, ImageProvider.fromBitmap(bitmap1)).setUserData("Копылова, 66");
            // Свободный проспект, 42
            Point mappoint2= new Point(56.023611, 92.807009);
            pointCollection.addPlacemark(mappoint2, ImageProvider.fromBitmap(bitmap1)).setUserData("Свободный проспект, 42");
            // Тотмина, 6
            Point mappoint3= new Point(56.031553, 92.775766);
            pointCollection.addPlacemark(mappoint3, ImageProvider.fromBitmap(bitmap1)).setUserData("Тотмина, 6");
            // Калинина, 82
            Point mappoint4= new Point(56.042296, 92.783743);
            pointCollection.addPlacemark(mappoint4, ImageProvider.fromBitmap(bitmap1)).setUserData("Калинина, 82");
            // Карла Маркса, 93
            Point mappoint5 = new Point(56.009157, 92.873260);
            pointCollection.addPlacemark(mappoint5, ImageProvider.fromBitmap(bitmap3)).setUserData("Карла Маркса, 93");
            // Карла Маркса, 49
            Point mappoint6 = new Point(56.010658, 92.880486);
            pointCollection.addPlacemark(mappoint6, ImageProvider.fromBitmap(bitmap3)).setUserData("Карла Маркса, 49");
            // Красномосковская улица, 42
            Point mappoint7 = new Point(56.023089, 92.820814);
            pointCollection.addPlacemark(mappoint7, ImageProvider.fromBitmap(bitmap3)).setUserData("Красномосковская улица, 42");
            // Татышев
            Point mappoint8 = new Point(56.033744, 92.942090);
            pointCollection.addPlacemark(mappoint8, ImageProvider.fromBitmap(bitmap2)).setUserData("Татышев");
            // Покровский парк
            Point mappoint9 = new Point(56.024426, 92.863553);
            pointCollection.addPlacemark(mappoint9, ImageProvider.fromBitmap(bitmap2)).setUserData("Покровский парк");
            // 2-й микрорайон
            Point mappoint10 = new Point(56.046360, 92.903389);
            pointCollection.addPlacemark(mappoint10, ImageProvider.fromBitmap(bitmap2)).setUserData("2-й микрорайон");
            // Айвазовского, 37
            Point mappoint11 = new Point(56.023374, 93.042646);
            pointCollection.addPlacemark(mappoint11, ImageProvider.fromBitmap(bitmap2)).setUserData("Айвазовского, 37");
            // Свободный, 29
            Point mappoint12 = new Point(56.023459, 92.822281);
            pointCollection.addPlacemark(mappoint12, ImageProvider.fromBitmap(bitmap2)).setUserData("Свободный, 29");
        }

//        button1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                zoomIn();
//            }
//        });
//        button2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                reduceZoom();
//            }
//        });
    }

    @Override
    protected void onStop() {
        mapView.onStop();
        MapKitFactory.getInstance().onStop();
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
        MapKitFactory.getInstance().onStart();
        mapView.onStart();
    }

    public void zoomIn(View v) {
        zoom += 1;
        String strZoom = String.valueOf(zoom);
        String login = getIntent().getStringExtra("LOGIN_USER");
        Intent intent = new Intent(this, MapKit.class);
        intent.putExtra("INFO_MAP_POINT", infoMapPoint);
        intent.putExtra("zoom", strZoom);
        intent.putExtra("LOGIN_USER", login);
        startActivity(intent);
//        recreate();
    }

    public void reduceZoom(View v) {
        zoom -= 1;
        String strZoom = String.valueOf(zoom);
        String login = getIntent().getStringExtra("LOGIN_USER");
        Intent intent = new Intent(this, MapKit.class);
        intent.putExtra("INFO_MAP_POINT", infoMapPoint);
        intent.putExtra("zoom", strZoom);
        intent.putExtra("LOGIN_USER", login);
        startActivity(intent);
//        recreate();
    }

    public  void goMenu(View v) {
        String login = getIntent().getStringExtra("LOGIN_USER");
        Intent intent = new Intent(this, Menu.class);
        intent.putExtra("LOGIN_USER", login);
        startActivity(intent);
    }

    public static Bitmap getBitmapFromVectorDrawable(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(drawable)).mutate();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    @Override
    public boolean onMapObjectTap(@NonNull MapObject mapObject, @NonNull Point point) {
        View view = (View)findViewById(R.id.view6);
        TextView textView = (TextView)findViewById(R.id.textView8);
        textView.setText(mapObject.getUserData().toString());
        view.setBackgroundColor(getResources().getColor(R.color.white));
        return true;
    }
}