# ОПИСАНИЕ:
---
Данное приложение поможет вам стать героем своей планеты. Экогород - это приложение-помощник для ответственного образа жизни, которое позволяет быстро и просто найти социальные и экологические сервисы в Красноярске.

# ИСПОЛЬЗОВАНИЕ:
---
При первом входе вы попадаете на приветственную страницу. Данная страница отображается только при первом входе в приложение.

---
![ПервыйВход](/screen/1.jpg)

После нажатия кнопки "Начать спасение", вы попадаете на страницу входа. Здесь вы можете войти в свой профиль, если уже зарегестрированы, или перейти на страницу регистрации, если еще не зарегестрированы. При входе в профиль приложение запоминает ваше имя и пароль, поэтому при следующем входе в приложение, вам не нужно будет вводить заново данные.

---
![Вход](/screen/2.jpg)
    
При нажатии на надпись "Зарегистрироваться ->" вы попадаете на данную страницу, где создаете свой профиль.

---
![Регистрация](/screen/3.jpg)
    
При успешном входе вы попадаете на главную страницу. Здесь вы увидете самые востребованные категории и кнопку "Все категории". При нажатии на которую вы перейдете на страницу со всеми категориями(будет описана ниже). В левом вернем углу находится три полоски, нажав на них вы попадете в меню приложения. Данные три полоски символизируют переход в меню из любой другой страницы.

---
![Главная](/screen/4.jpg)
    
В меню вы можете перейти на любую страницу приложения (Профиль, Главная, Все категории, Карта, Новости). Главная страница была рассмотрена выше.

---
![Меню](/screen/5.jpg)
    
При переходе в профиль вы попадаете на страницу, где отображено ваше имя профиля и ваши достижения. Также имеется три кнопки "Редактировать", "Удалить", "Выйти". При нажатии кнопки "Удалить" вы удаляете свой аккаунт и возвращаетесь на страницу входа. При нажатии кнопки "Выйти" вы также попадаете на страницу входа, только данные вашего профиля сохраняются в локальной базе данных. Поэтому вы сможете войти в профиль под теми же данными.

---
![Профиль](/screen/6.jpg)
    
При нажатии кнопки "Редактировать" вы попадаете на страницу редактирования данных. Здесь вы можете поменять имя и пароль. Новые данные сохранятся только при нажатии на кнопку "Сохранить изменения".

---
![Редактирование](/screen/7.jpg)
    
Если на странице меню или на главной странице вы выбираете "Все категории", то попадаете на страницу где представлены все категории в "Переработке". При нажатии на кнопку "Животные" или "Помощь" вы увидете все категории в этих направлениях. 

---
![Категории1](/screen/8.jpg)
![Категории2](/screen/9.jpg)
![Категории3](/screen/10.jpg)
    
При нажатии на любую категорию вы попадаете на страницу с картой, где отмечены метки. Каждая метка показывает где расположена локация для данной категории.

---
![КартаБатарейки](/screen/12.jpg)

Сверху есть две кнопки, которые помогут увеличить или уменьшить карту. Также при нажатии на метку появится описание внизу.

---
![КартаБатарейкиОписание](/screen/11.jpg)
![КартаПомощьОписание](/screen/13.jpg)

Если на странице меню вы выбираете "Карта", то попадаете на страницу карты, где представлены все метки на все категории.

---
![Карта](/screen/14.jpg)

Если на странице меню вы выбираете "Новости", то попадаете на страницу новости, где открыта страница с официального сайта с национальными новостями из мира экологии. Вы можете просматривать данные новости прямо из приложения "Экогород".

---
![Карта](/screen/15.jpg)

---
**Удачи герой, давай спасем планету вместе!**